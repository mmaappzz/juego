
var herramienta = function (_id, _nombre, _costo, _cps) {
	var _this = this;
	var horaAnterior = new Date().getTime();
	this.nombre = _nombre;
	this.id = _id;
	this.costo = _costo;
	this.cps = _cps;
	this.cantidad = 0;

	var Agregar_onClick		= function(event) { _this.Agregar(1); };
	var Quitar_onClick		= function(event) { log("Quitar"); };

	this.verCosto		= function() { return this.costo; };
	this.verNombre		= function() { return this.nombre; };
	this.verCps			= function() { return this.cps; };
	this.defMulti		= function(multi) { this.cps *= multi };
	
	
	var div = document.createElement("div");
	div.id = _id;
	div.setAttribute("class", "herramienta")
	var Nombre = document.createElement("div");
	Nombre.innerHTML = _nombre;
	Nombre.setAttribute("class", "Nombre");
	Nombre.id = "Nombre_" + _id;
	div.appendChild(Nombre);

	div.appendChild(crearCosto("Ladrillo", div.id, Numeros(this.costo)));
	div.appendChild(crearEtiqueta("Cantidad", div.id));

	var btnAgregar = document.createElement("button");
	btnAgregar.id = "Agregar_" + _id;
	btnAgregar.innerHTML = "Agregar";
	btnAgregar.addEventListener ("click", Agregar_onClick, false)

	var btnQuitar = document.createElement("button");
	btnQuitar.id = "Quitar_" + _id;
	btnQuitar.innerHTML = "Quitar";
	btnQuitar.addEventListener ("click", Quitar_onClick, false)

	var cont_btn = document.createElement("div");
	cont_btn.appendChild(btnAgregar);
	//cont_btn.appendChild(btnQuitar);
	cont_btn.setAttribute("class", "botones");
	div.appendChild(cont_btn);
	document.getElementById("contenedor_herramientas").appendChild(div);
	this.Agregar = function (cant) {
		if (this.costo <= j.verLadrillos()) {
			log("Comprar " + this.nombre + " a " + Numeros(this.costo) + " ladrillos");
			this.cantidad += cant;
			j.defCPS(cant * this.cps);
			j.quitarLadrillos(this.costo * cant);
			this.costo = this.costo * 1.1;
		}
	};
	this.defCantidad = function (cant) {
		this.cantidad = cant;
		this.costo = this.costo * Math.pow(1.1, cant);
	};
	this.calcular = function () {
		document.getElementById("Ladrillo_" + this.id).innerHTML = fix(this.verCosto(), "money");
		document.getElementById("Cantidad_" + this.id).innerHTML = fix(this.cantidad, "money");
		if ( this.verCosto() <= j.verLadrillos() ) {
			document.getElementById("Agregar_" + this.id).disabled = false;
		} else {
			document.getElementById("Agregar_" + this.id).disabled = true;
		}
	};
}