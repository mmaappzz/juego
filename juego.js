var cantClick;
var j;

var Nombre = "Juego";
var Version = "1.0";


iniciar = function () {
	log("inicio");
	j = new juego(0);
	cantClick = 1;
	var ciclo = function () {
		j.dibujar();
		requestAnimationFrame(ciclo);
	}
	requestAnimationFrame(ciclo);
}
var agregar = function (cant) {
	j.agregarLadrillos(cant);
}
var guardar = function () {
	j.guardar (j.verLadrillos(), j.verTotalLadrillos(), j.h1.cantidad, j.h2.cantidad, j.h3.cantidad, j.h4.cantidad, j.verHoraInicial())
}
var reset = function () {
	//j.guardar (0, 0, 0, 0, 0, 0, new Date().getTime())
	j.reset();
	location.reload();
}
var juego = function (_ladrillos) {
	log("new");
	var datos = JSON.parse(localStorage.getItem("datos"));
	var horaAnterior = new Date().getTime();
	var horaGuardar = new Date().getTime();
	var horaInicial = new Date().getTime();
	//log(localStorage.getItem("datos"));
	var cps = 0
	this.h = [];
	
	this.h.push( new herramienta("h0", "Herramienta1", 10, .1) );
	this.h.push( new herramienta("h1", "Herramienta2", 100, 1) );
	this.h.push( new herramienta("h2", "Herramienta3", 1000, 10) );
	this.h.push( new herramienta("h3", "Herramienta4", 10000, 100) );
	this.h.push( new herramienta("h4", "Herramienta5", 100000, 1000) );
	this.h.push( new herramienta("h5", "Herramienta6", 1000000, 10000) );
	this.h.push( new herramienta("h6", "Herramienta7", 10000000, 100000) );
	this.h.push( new herramienta("h7", "Herramienta8", 100000000, 1000000) );
	
	this.m = [];
	
	this.m.push( new mejora("m0", "Mejora H0", 20, 0, 2) );

	var ladrillos = _ladrillos;
	var totalLadrillos = _ladrillos;

	console.log (this.h);
	
	this.agregarLadrillos	= function(cant) { ladrillos += cant; totalLadrillos += cant; };
	this.quitarLadrillos	= function(cant) { ladrillos -= cant; };
	this.verLadrillos		= function() { return ladrillos; };
	this.verTotalLadrillos	= function() { return totalLadrillos; };
	this.verCPS				= function() { 
		for (i = 0; i < this.h.length; i++) {
			this.h[i].verCPS();
		}

	return cps; 
	};
	this.defCPS				= function(cant) { cps += cant; };
	this.verHoraInicial		= function() {return horaInicial};
	
	this.dibujar = function() {
		this.calcular();
		document.getElementById("cant_ladrillos_s").innerHTML = Numeros(this.verCPS());
		document.getElementById("cant_ladrillos").innerHTML = Numeros(ladrillos);
		document.getElementById("total_ladrillos").innerHTML = Numeros(totalLadrillos);
		hora = new Date().getTime();
		tiempo = hora - horaInicial;
		document.getElementById("tiempo").innerHTML = Hora(tiempo);
	}
	this.calcular = function() {
		var hora = new Date().getTime();
		var difHora =  (hora - horaAnterior);
		for (i = 0; i < this.h.length; i++) {
			this.h[i].calcular();
		}
		for (i = 0; i < this.m.length; i++) {
			this.m[i].calcular();
		}
		horaAnterior = hora;
		j.agregarLadrillos ( (this.verCPS()/1000) * difHora );
		/*if ( hora - horaGuardar >= 60 * 1000) {
			guardar();
			horaGuardar = hora;
		}*/
	};
	this.guardar = function (l, t, h1, h2, h3, h4, tiempo) {
		var trans = finTransicion();
		trans.addEventListener("transitionend", finTransicion, true);
		var grab = document.getElementById("grabando")
		grab.className = "visible";
		var grabar = {
			l: l,
			t: t,
			h1: h1,
			h2: h2,
			h3: h3,
			h4: h4,
			tiempo: tiempo
		}
		localStorage.setItem("datos", JSON.stringify(grabar));
		log("Grabando - " + document.getElementById("tiempo").innerHTML);
		//grab.className = "oculto";
	};
	finTransicion = function () {
		var grab = document.getElementById("grabando");
		if (grab.className == "visible") {
			grab.className = "oculto";
		}
		return grab;
	}
	this.reset = function () {
		localStorage.clear();
	};
	
	
	//console.log (this.h);
	if (datos !== null) {
		horaInicial = datos.tiempo;
		ladrillos = datos.l;
		totalLadrillos = datos.t;
		for (i = 0; i<h.length; i++) {
			this.h[i].defCantidad(datos.h[i]);
			this.defCPS(datos.h1 * this.h[i].verCps());
		}
	}
}
function crearEtiqueta(texto, id) {
	var etiqueta = document.createElement("div");
	var label = document.createElement("label");
	var span = document.createElement("span");
	label.innerHTML = texto + ": ";
	span.innerHTML = 0;
	span.id = texto + "_" + id;
	etiqueta.setAttribute("class", "etiqueta");
	etiqueta.appendChild(label);
	etiqueta.appendChild(span);
	return etiqueta;
};
function crearCosto(texto, id, costo) {
	var Costo = document.createElement("div");
	var imagen = document.createElement("img");
	imagen.setAttribute("src", "img/ladrillo.svg");
	imagen.setAttribute("title", "texto");
	imagen.style.width = "24px";
	var span = document.createElement("span");
	span.innerHTML = costo;
	span.id = texto + "_" + id;
	Costo.setAttribute("class", "Requisitos");
	Costo.appendChild(imagen);
	Costo.appendChild(span);
	return Costo;
};

function Numeros (num) {
	return FormatNumberBy3(Math.round(num*10)/10);
}
function Hora(tiempo) {
	var segundos = parseInt(tiempo / 1000, 10);
    var hrs = Math.floor(segundos / 3600);
    var min = Math.floor((segundos - (hrs * 3600)) / 60);
    var seg = segundos - (hrs * 3600) - (min * 60);

    if (hrs < 10) {hrs = "0" + hrs;}
    if (min < 10) {min = "0" + min;}
    if (seg < 10) {seg = "0" + seg;}
    return hrs + ':' + min + ':' + seg;
}

/*var getRequestAnimationFrame = function () {
	return window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		window.msRequestAnimationFrame ||
		function ( callback ){
			window.setTimeout(enroute, 100);
		};
};*/


// function to format a number with separators. returns formatted number.
// num - the number to be formatted
// decpoint - the decimal point character. if skipped, "." is used
// sep - the separator character. if skipped, "," is used
function FormatNumberBy3(num, decpoint, sep) {
  // check for missing parameters and use defaults if so
  if (arguments.length == 2) {
    sep = ".";
  }
  if (arguments.length == 1) {
    sep = ".";
    decpoint = ",";
  }

  x = Math.floor(num);
  y = Math.floor((num - x) * 10);
  x = x.toString();
  y = y.toString();
  z = "";


  if (typeof(x) != "undefined") {
    // reverse the digits. regexp works from left to right.
    for (i=x.length-1;i>=0;i--)
      z += x.charAt(i);
    // add seperators. but undo the trailing one, if there
    z = z.replace(/(\d{3})/g, "$1" + sep);
    if (z.slice(-sep.length) == sep)
      z = z.slice(0, -sep.length);
    x = "";
    // reverse again to get back the number
    for (i=z.length-1;i>=0;i--) x += z.charAt(i);
    // add the fraction back in, if it was there
    if (typeof(y) != "undefined" && y.length > 0) x += decpoint + y;
  }
  return x;
}