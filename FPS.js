FPS = function() {
	var horaAnterior = new Date().getTime();
	var cuadros = 0;
	this.id = "Valor"
	document.getElementById("divFPS").appendChild(crearEtiqueta("FPS", this.id));
	this.calcular = function () {
		var hora = new Date().getTime();
		var difHora = (hora - horaAnterior);
		if (difHora >= 1000) {
			document.getElementById("FPS_Valor").innerHTML = cuadros;
			cuadros = 0;
			horaAnterior = hora;
		}
		cuadros++;
	};	
};